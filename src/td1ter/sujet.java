package td1ter;

public class Sujet {
	private String titre;
	private String resume;
	private int codeSujet;
	
	public Sujet(String titre, String resume, int codeSujet) {
		this.titre = titre;
		this.resume = resume;
		this.codeSujet = codeSujet;
	}
	
}
