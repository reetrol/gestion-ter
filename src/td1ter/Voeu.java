package td1ter;

public class Voeu {
	
	private Sujet sujetSouhaite;
	private Group groupe;
	private int priorite;
	
	public Voeu(Sujet sujetSouhaité,Group groupe, int priorite){
		this.sujetSouhaite = sujetSouhaite;
		this.groupe = groupe;
		this.priorite = priorite;
	}
}
