package td1ter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestGroupeSujet {
	
	

	
	Group groupe1;
	Group groupe2;
	Sujet sujetA;
	Sujet sujetB;
	
	@BeforeEach
	public void initialisationSujetsGroupes() {
		groupe1 = new Group(1);
		groupe2 = new Group(2);
		sujetA = new Sujet("Sujet A","resume Sujet A",1);
		sujetB = new Sujet("Sujet B","resume Sujet B",2);
	}
	
	
	@Test
	void testAffectation() {
		groupe1.AffecteSujet(sujetA);
		groupe2.AffecteSujet(sujetB);
		assertEquals(sujetA , groupe1.getSujet());
		assertEquals(groupe1 , sujetA.getGroup());
		
		assertEquals(sujetB,groupe2.getSujet());
		assertEquals(groupe2 , sujetB.getGroup());
		
	}

}
