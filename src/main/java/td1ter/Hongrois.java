package td1ter;
import java.util.List;

public interface Hongrois {
	
	public void setHauteur(int hauteur);

	public void setLargeur(int largeur);

	public void setAdjacenceList(List<List<Integer>> adjListe);

	public List<List<Integer>> affectation(int phaseNumber);
	}
